<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TiendaProducto extends Model
{
    use HasFactory;

    protected $table = 'tienda_producto';

     /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'producto_id',
        'tienda_id',
        'cantidad',
    ];

    public function producto() {
        return $this->belongsTo(Producto::class, 'producto_id');
    }

    public function tienda() {
        return $this->belongsTo(Tienda::class, 'tienda_id');
    }
}
