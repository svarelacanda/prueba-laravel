<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nombre',
    ];
    

    /**
     * Devuelve las tiendas del producto
     */
    public function tiendas() {
        return $this->belongsToMany(Tienda::class, 'tienda_producto', 'producto_id', 'tienda_id')
                    ->withPivot('cantidad');
    }
}
