<?php

namespace App\Exceptions;

use Exception;

class ProductoNotFoundException extends Exception
{
    public function render($request, Throwable $exception)
    {       
        parent::report($exception);
    }
}
