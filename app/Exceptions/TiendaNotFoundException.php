<?php

namespace App\Exceptions;

use Exception;

class TiendaNotFoundException extends Exception
{
    public function render($request, Throwable $exception)
    {       
        parent::report($exception);
    }
}
