<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VentaProductoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tienda_id' => 'required|integer',
            'producto_id' => 'required|integer',
            'unidades' => 'required|integer'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'tienda_id.required' => 'El identificador de la tienda es obligatorio',
            'producto_id.required' => 'El identificador del producto es obligatorio',
            'unidades.required' => 'El número de unidades vendidas del producto es obligatorio',
        ];
    }
}
