<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tienda;
use App\Models\Producto;
use App\Http\Requests\VentaProductoRequest;
use App\Exceptions\TiendaNotFoundException;
use App\Exceptions\ProductoNotFoundException;

class TiendaProductoController extends Controller
{
    public function venta(VentaProductoRequest $request) {

        try {

            $tienda = Tienda::find($request->tienda_id);
            if( empty($tienda) ) {
                throw new TiendaNotFoundException("La tienda no existe.", 1);
            }

            $producto = Producto::find($request->producto_id);
            if( empty($producto) ) {
                throw new ProductoNotFoundException("El producto no existe.", 1);
            }
            
            $cantidad = ($tienda->productos()->find($producto))->pivot->cantidad;
            $cantidad -= $request->unidades;
            if( $cantidad < 0 ) {
                return response()->json(['message' => 'No existen unidades suficientes disponibles.'], 500);
            }
            $tienda->productos()->updateExistingPivot($producto->id, ['cantidad' => $cantidad]);

        } catch (TiendaNotFoundException $e) {
            return response()->json(['message' => $e->getMessage()], 404);    
        } catch (ProductoNotFoundException $e) {
            return response()->json(['message' => $e->getMessage()], 404);    
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }

        if( $cantidad <= 5 ) {
            return response()->json("Se ha realizado la venta correctamente. El stock es igual o menor a 5 unidades.", 200);
        }
        return response()->json("Se ha realizado la venta correctamente.", 200);

    }
}
