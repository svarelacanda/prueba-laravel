<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Tienda;
use App\Models\Producto;
use App\Http\Requests\StoreTiendaRequest;
use App\Exceptions\TiendaNotFoundException;

class TiendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $tiendas = Tienda::select('nombre')
                        ->withCount('productos')
                        ->get(); 
            

        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }

        return response()->json($tiendas, 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTiendaRequest $request)
    {

        try {
            $tienda = new Tienda;
            $tienda->nombre = $request->nombre; 
            $tienda->save();
    
            $productos =  $request->productos ?? [];
            if( empty($productos)) {    
                return response()->json('La tienda se ha creado correctamente.', 200);
            }

            $validator = Validator::make($request->all(), [
                'productos.*.nombre' => 'required|string|max:100',
                'productos.*.cantidad' => 'required|integer',
            ]);
            
            foreach($request->productos as $info_producto) {
                $info_producto = (object) $info_producto;
                $producto = new Producto;
                $producto->nombre = $info_producto->nombre;
                $producto->save();

                $tienda->productos()->attach($producto->id, ['cantidad'=>$info_producto->cantidad]);
            }


        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }

        return response()->json('La tienda y los productos se han creado correctamente.', 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {

            $tienda = Tienda::where('id', $id)
                    ->select('id', 'nombre')
                    ->with('productos:nombre')
                    ->first();

            if( empty($tienda) ) {
                throw new TiendaNotFoundException("La tienda no existe.", 1);
            }

        } catch (TiendaNotFoundException $e) {
            return response()->json(['message' => $e->getMessage()], 404);    
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }

        return response()->json($tienda, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTiendaRequest $request, $id)
    {
        try {
            $tienda = Tienda::find($id);
           
            if( empty($tienda) ) {
                throw new TiendaNotFoundException("La tienda no existe.", 1);
            }

            $tienda->nombre = $request->nombre; 
            $tienda->save();

        } catch (TiendaNotFoundException $e) {
            return response()->json(['message' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }

        return response()->json('La tienda se ha actualizado correctamente.', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $tienda = Tienda::find($id);
            
            if( empty($tienda) ) {
                throw new TiendaNotFoundException("La tienda no existe.", 1);
            }

            $tienda->delete();

        } catch (TiendaNotFoundException $e) {
            return response()->json(['message' => $e->getMessage()], 404);    
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }

        return response()->json(['message' => 'La tienda se ha eliminado correctamente.'], 200);
    }
}
