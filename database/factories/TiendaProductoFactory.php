<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class TiendaProductoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'producto_id' => \App\Models\Producto::factory(),
            'tienda_id' => \App\Models\Tienda::factory(),
            'cantidad' => $this->faker->numberBetween(1,1000),
        ];
    }
}
